package io.fract.designer.streamControl.webSocketFront;

import java.util.ArrayList;

import org.vertx.java.core.AsyncResult;
import org.vertx.java.core.AsyncResultHandler;
import org.vertx.java.core.Handler;
import org.vertx.java.core.SimpleHandler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.http.ServerWebSocket;
import org.vertx.java.core.logging.Logger;
import org.vertx.java.deploy.Verticle;

public class WebSocketFront extends Verticle {

	public ArrayList<ServerWebSocket> serverWebSockets = new ArrayList<ServerWebSocket>();
	
	public void start() {
		vertx.createHttpServer()
				.websocketHandler(new Handler<ServerWebSocket>() {

					Logger logger = container.getLogger();

					public void handle(final ServerWebSocket ws) {
						logger.info("WebSocket Connected");
						
						serverWebSockets.add(ws);
						
						if (ws.path.equals("/binaryStream")) {
							ws.dataHandler(new Handler<Buffer>() {
								public void handle(Buffer data) {
									//ws.writeTextFrame(data.toString());
									vertx.fileSystem().readFile("sierpinski_512x512.png.webp", new AsyncResultHandler<Buffer>() {
									    public void handle(AsyncResult<Buffer> ar) {
									        if (ar.exception == null) {                
									        	logger.info("File contains: " + ar.result.length() + " bytes");  
									        	ws.writeBinaryFrame(ar.result);
									        } else {
									        	logger.error("Failed to read", ar.exception);
									        }
									    }
									});
								}
							});
						} else {
							logger.info("WebSocket Rejected");
							ws.reject();
						}
					}
				}).requestHandler(new Handler<HttpServerRequest>() {
					Logger logger = container.getLogger();
					
					public void handle(final HttpServerRequest req) {

						if (req.path.equals("/")) {
							logger.info("Serving html");
							req.response.sendFile("pictureEater.html");
						}else{
							req.pause();
							final Buffer tempBuffer = new Buffer();
							
							req.dataHandler(new Handler<Buffer>() {

								@Override
								public void handle(Buffer incomingBuffer) {
									tempBuffer.appendBuffer(incomingBuffer);
								}
								
							});
									
									
							 req.endHandler(new SimpleHandler() {
								
								@Override
								protected void handle() {
									req.response.end();
									logger.info("Uploaded " + tempBuffer.length() + " bytes");
									
									//NB code is temp for multi client relay thing, this should be changed to not include 
									// .copy() in the prod code for single clients only
									
									for (int i = 0; i < serverWebSockets.size(); i++) {
										serverWebSockets.get(i).writeBinaryFrame(tempBuffer.copy());
									}
									
								}
							 });
					           // pump.start();
					            req.resume();
						}
					}
				}).listen(1500);
	}
}